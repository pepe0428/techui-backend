# Imagen Docker base/incial
FROM  node:latest


#Crear directorio del contenedor Docker
WORKDIR /docker-apitechu

#Copia archivos del proyecto en el directorio del contenedor
ADD . /docker-apitechu

#Exponer el puerto del contenedor (mismo definido en nuestra API)
EXPOSE 3000

#lanzar comandos para ejecutar nuestra app
CMD [ "npm","run","start-pro" ]
