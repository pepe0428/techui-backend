const global=require('../global.js')
users_file=global.users_file;
fs=global.fs;


function getUsers(req,res){

    console.log(global.URL_BASE+'users');
    var httpClient = global.requestJSON.createClient(global.baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + global.apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            console.log(body.length);
            response = body;
          } else {
            response = {"msg" : "Ningún elemento 'user'."}
            res.status(404);
          }
        }
        res.send(response);
      });

  /*
  console.log('GET con Query String');
  console.log('GET Filtrar por cantidad');
  console.log("GET Con cantidad = "+req.query.cantidad);

  if(req.query.cantidad!=null && req.query.cantidad!=0){
    var users_filtrados= new Array();
    for (let index = 0; index < req.query.cantidad; index++) {
      users_filtrados.push(users_file[index]);
    }
    console.log(users_filtrados.length)
    resp.send(users_filtrados);
  }else{
    resp.send(users_file);
  }*/
}


function getUserById(req, resp){
    let ID=req.params.id;
    var user=null;
    console.log("GET Con id = "+req.params.id);
    console.log(users_file.length);
    users_file.forEach(user =>  {
      if(user.ID==ID){
        resp.send(user);
      }
    });
    if(user==null){
      resp.send({"msg":"Usuario no encontrado"})
    }
   };

//POST users
  function crearUser(req,res){
    var clienteMlab = global.requestJSON.createClient(global.baseMLabURL);
    console.log(req.body);
    console.log(global.apikeyMLab);docker
    clienteMlab.get('user?'+global.apikeyMLab,
    function(erros,respuestaMLab, body){
      newId=body.length+1;
      console.log("newId"+newId);
      var newUser={
        "id":newId,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password":req.body.password
      };
      clienteMlab.post(
        "user?"+global.apikeyMLab,newUser,
      function(error, respuestaMLab,body){
        console.log(body);
        res.status(201);
        res.send(body);
      })
    })
    /*console.log('POST de users');
    console.log('Nuevo usuario:' + req.body);
    console.log('Nuevo usuario:' + req.body.first_name);
    let newID=users_file.length+1;
    console.log(newID)
    let newUser={
      "id":newID,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    users_file.push(newUser);
    console.log("nuevo usuario"+ newUser.first_name);
    console.log(users_file.length);
    resp.send({"msg":"Usuario creado"})*/

  }


//PUT users
  function actualizarUserxID(req,res){
      var id = req.params.id;
      console.log(id)
      var queryStringID = 'q={"id":' + id + '}&';
      var clienteMlab = global.requestJSON.createClient(global.baseMLabURL);
      clienteMlab.get('user?'+ queryStringID + global.apikeyMLab,
        function(error, respuestaMLab, body) {
         var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
         console.log(req.body);
         console.log(cambio);
         clienteMlab.put(global.baseMLabURL +'user?' + queryStringID + global.apikeyMLab, JSON.parse(cambio),
          function(error, respuestaMLab, body) {
            console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
           //res.status(200).send(body);
           res.send(body);
          });
        });  
    /*
    console.log('PUT de users');
    console.log('Nuevo usuario:' + req.body);
    console.log('Nuevo usuario:' + req.body.first_name);
    let ID=req.body.id;
    let newUser={
      "ID":req.body.id,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }

    users_file[ID-1]=newUser;
    console.log("nuevo usuario"+ newUser.first_name);
    console.log(users_file.length);
    resp.send({"msg":"Actualizacion con Exito"});*/
  };

//(_id.$oid)
  function actualizarUserxOid(req,res){
    var id = req.params.id;
    let userBody = req.body;
    var queryString = 'q={"id":' + id + '}&';
    var httpClient = global.requestJSON.createClient(global.baseMLabURL);
    httpClient.get('user?' + queryString + global.apikeyMLab,
      function(err, respuestaMLab, body){
        let response = body[0];
        console.log(body);
        //Actualizo campos del usuario
        let updatedUser = {
          "id" : req.body.id,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };//Otra forma simplificada (para muchas propiedades)
        // var updatedUser = {};
        // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
        // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
        // PUT a mLab
        httpClient.put('user/' + response._id.$oid + '?' + global.apikeyMLab, updatedUser,
          function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                response = {
                  "msg" : "Error actualizando usuario."
                }
                res.status(500);
            } else {
              if(body.length > 0) {
                response = body;
              } else {
                response = {
                  "msg" : "Usuario actualizado correctamente."
                }
                res.status(200);
              }
            }
            res.send(response);
          });
      });
  }

  //DELETE
    function eliminarUser(req,resp){

      var id = Number.parseInt(req.params.id);
      console.log(id)
      var queryStringID = 'q={"id":' + id + '}&';
      var clienteMlab = global.requestJSON.createClient(global.baseMLabURL);
      clienteMlab.put('user?'+ queryStringID + global.apikeyMLab,
        function(error, respuestaMLab, body) {
         var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
         console.log(req.body);
         console.log(cambio);
         clienteMlab.put(global.baseMLabURL +'user?' + queryStringID + global.apikeyMLab, JSON.parse(cambio),
          function(error, respuestaMLab, body) {
            console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
           //res.status(200).send(body);
           res.send(body);
          });
        });
      /*console.log('Delete de users.');
      let pos=req.params.id;
      let respuesta='';
      if(users_file[pos-1]==undefined){
        respuesta={"msg":"No existe este usuario"};
      }else{
        users_file.splice(pos-1,1);
        respuesta={"msg":"Usuario eliminado"};
      }

      resp.send(respuesta)*/
     }
  

  



  //Usuario Activos
 
  function usuarioActivos(req,resp){
    console.log('GET con Query String');
    let users_activo= new Array();
    users_file.forEach(user =>  {
      if(user.logged==true ){
        //break;
        users_activo.push(user);
      }
    });

    resp.send(users_activo);
  }

module.exports={
    getUsers,
    getUserById,
    crearUser,
    eliminarUser,
    usuarioActivos,
    actualizarUserxID,
    actualizarUserxOid
}



