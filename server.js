var express = require('express');

//const cors=require('cors');
const global=require('./global.js');
var app = express();

//const URL_BASE='/techu-peru/v1/';
app.use(global.bodyParser.json());

//app.use(cors());
//app.options('*',cors());

const user_controller=require('./controllers/users');
const auth_controller=require('./controllers/auth');

//Peticion GET Users
/*app.get(URL_BASE+'users', function (req, res) {
  res.status(201).send(users_file);
});*/

//Peticion GET User con ID
//app.get(global.URL_BASE+'users/:id',user_controller.getUserById);
/*app.get(global.URL_BASE+'users/:id',function(req, resp){
  let ID=req.params.id;
  var user=null;
  console.log("GET Con id = "+req.params.id);
  console.log(users_file.length);
  users_file.forEach(user =>  {
    if(user.ID==ID){
      resp.send(user);
    }
  });
  if(user==null){
    resp.send({"msg":"Usuario no encontrado"})
  }
 });*/

//  GET users con Query String
//app.get(global.URL_BASE+'users',user_controller.getUsers);
/*
app.get(global.URL_BASE+'users',
  function(req,resp){
    console.log('GET con Query String');
    console.log('GET Filtrar por cantidad');
    console.log("GET Con cantidad = "+req.query.cantidad);

    if(req.query.cantidad!=null && req.query.cantidad!=0){
      var users_filtrados= new Array();
      for (let index = 0; index < req.query.cantidad; index++) {
        users_filtrados.push(users_file[index]);
      }
      console.log(users_filtrados.length)
      resp.send(users_filtrados);
    }else{
      resp.send(users_file);
    }
   
})
*/



//POST users
//app.post(global.URL_BASE+'users',user_controller.crearUser);
/*app.post(global.URL_BASE+'users',
  function(req,resp){
    console.log('POST de users');
    console.log('Nuevo usuario:' + req.body);
    console.log('Nuevo usuario:' + req.body.first_name);
    let newID=users_file.length+1;
    console.log(newID)
    let newUser={
      "id":newID,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    users_file.push(newUser);
    console.log("nuevo usuario"+ newUser.first_name);
    console.log(users_file.length);
    resp.send({"msg":"Usuario creado"})
  })
*/

//PUT users
//app.put(global.URL_BASE+'users',user_controller.actualizarUser);
/*app.put(global.URL_BASE+'users',
  function(req,resp){
    console.log('PUT de users');
    console.log('Nuevo usuario:' + req.body);
    console.log('Nuevo usuario:' + req.body.first_name);
    let ID=req.body.id;
    let newUser={
      "ID":req.body.id,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }

    users_file[ID-1]=newUser;
    console.log("nuevo usuario"+ newUser.first_name);
    console.log(users_file.length);
    resp.send({"msg":"Actualizacion con Exito"});
  });
*/

  //DELETE
  //app.delete(global.URL_BASE+ 'users/:id',user_controller.eliminarUser);
  /*app.delete(global.URL_BASE+ 'users/:id',
    function(req,resp){
      console.log('Delete de users.');
      let pos=req.params.id;
      let respuesta='';
      if(users_file[pos-1]==undefined){
        respuesta={"msg":"No existe este usuario"};
      }else{
        users_file.splice(pos-1,1);
        respuesta={"msg":"Usuario eliminado"};
      }

      resp.send(respuesta)
     }
  )*/

  //LOGIN
  //app.post(global.URL_BASE+'login',auth_controller.login);
  /*app.post(global.URL_BASE+'login', function(req, resp){
    console.log('Login');
    console.log(req.body.email);
    console.log(req.body.password);
    let idUsuario=0;
    users_file.forEach(user =>  {
      if(user.email==req.body.email && user.password==req.body.password){
        idUsuario=user.ID;
        user.logged=true;
        //break;
      }
    });

    writeUserDataToFile(users_file);
    if(idUsuario!=0){
      resp.send({"msg":"Login correcto", "id":idUsuario});
    }else{
      resp.send({"msg":"Login incorrecto"});
    }
  })*/

  //Logout
  //app.post(global.URL_BASE+'logout',auth_controller.logout);
  /*app.post(global.URL_BASE+'logout', function(req, resp){
    console.log('Logout');
    console.log(req.body.id);

    let idUsuario=0;
    users_file.forEach(user =>  {
      if(user.logged==true && user.ID==req.body.id){
        idUsuario=user.ID;
        delete user.logged;
        //break;
      }
    });
  
    if(idUsuario!=0){
      writeUserDataToFile(users_file);
      resp.send({"msg":"logout correcto", "id":idUsuario});
    }else{
      resp.send({"msg":"logout incorrecto"});
    }
  })
*/


  //Usuario Activos
  //app.get(global.URL_BASE+'loginActivo',user_controller.usuarioActivos);
  /*app.get(global.URL_BASE+'loginActivo',
  function(req,resp){
    console.log('GET con Query String');
    let users_activo= new Array();
    users_file.forEach(user =>  {
      if(user.logged==true ){
        //break;
        users_activo.push(user);
      }
    });

    resp.send(users_activo);
  })*/


  /**MLAB***/
  app.get(global.URL_BASE+'users', user_controller.getUsers);
  app.post(global.URL_BASE+'users',user_controller.crearUser);
  app.put(global.URL_BASE + 'users/:id',user_controller.actualizarUserxID);  
  // Petición PUT con id de mLab (_id.$oid)
  app.put(global.URL_BASE + 'usersmLab/:id',user_controller.actualizarUserxOid );
  app.delete(global.URL_BASE+ 'users/:id',user_controller.eliminarUser);

  app.post(global.URL_BASE+'login',auth_controller.login);

 
app.listen(global.port, function () {
  console.log('Example app listening on port 3000!');
});